package javax.xianfeng.jwt.api;

import javax.xianfeng.jwt.model.JwtUser;

import org.junit.Test;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * @since 2020/01/29 17:37:29
 */
public class JwtTest {

	@Test
	public void encode() {
		JwtUser user = new JwtUser("1001", "Admin");
		String token = JWT.create().withAudience(user.getId()).sign(Algorithm.HMAC256(user.getPassword()));
		System.out.println(token);
	}

	@Test
	public void decode() {
		String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxMDAxIn0.QyJXhdeq1mGNZQswD57KbGpJDOtTPJ-szyK5mMiHs8Y";
		DecodedJWT decode = JWT.decode(token);
		System.out.println(decode.getAudience());
		System.out.println(decode.getSignature());
	}

	@Test
	public void verify() {
		String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxMDAxIn0.QyJXhdeq1mGNZQswD57KbGpJDOtTPJ-szyK5mMiHs8Y";

		// 验证 token
		JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("123456")).build();
		try {
			jwtVerifier.verify(token);
		} catch (JWTVerificationException e) {
			e.printStackTrace();
		}
	}

}
