package com.hello123.service.demo;

import java.util.ArrayList;
import java.util.List;

import javax.xianfeng.SpringAntdApplicationTest;
import javax.xianfeng.antdpro.model.ApiResult;
import javax.xianfeng.antdpro.model.PagingApiResult;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.hello123.model.demo.User;

/**
 * @since 2020/01/28 13:02:36
 */
public class UserServiceTest extends SpringAntdApplicationTest {

	@Autowired
	private UserService userService;

	@Test
	public void get() {
		ApiResult result = userService.get(1L);
		System.out.println(result);
	}

	@Test
	public void list() {
		PagingApiResult result = userService.list(10, 1, null, null, null, "jack", null, null, null);
		System.out.println(result.getPagination());
		System.out.println(result.getList());
	}

	@Test
	public void save() {
		User user = new User();
		user.setName("user");
		user.setType("user");
		userService.save(user);
	}

	@Test
	public void update() {
		User user =(User) ((ApiResult) userService.get(1L)).getData();
		System.out.println(user);
		user.setName("root");
		userService.update(user);
	}

	@Test
	public void batchDelete() {
		List<Long> ids = new ArrayList<>();
		ids.add(13L);
		ids.add(14L);
		userService.batchDelete(ids);
	}

	@Test
	public void deleteByIdIn() {
		List<Long> ids = new ArrayList<>();
		ids.add(13L);
		ids.add(14L);
		userService.deleteByIdIn(ids);
	}

}
