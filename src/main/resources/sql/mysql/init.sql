DROP TABLE IF EXISTS T_USER;

CREATE TABLE T_USER (
ID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
NAME VARCHAR(20) NOT NULL COMMENT '用户名',
TYPE VARCHAR(10) NOT NULL COMMENT '类型',
SEX CHAR(1) COMMENT '性别',
AGE INT(3) DEFAULT 0 COMMENT '年龄',
BIRTHDAY DATE DEFAULT '1980-01-01' COMMENT '生日',
NOTE VARCHAR(50) COMMENT '介绍',
PRIMARY KEY (ID)
) COMMENT '用户信息表';

INSERT INTO T_USER(NAME, TYPE, SEX, NOTE) VALUES('Admin','admin','1','系统管理员');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('张三','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('李四','user','0');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('王五','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Jack','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Rose','user','0');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Joy','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Tom','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('John','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Lily','user','0');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Ben','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Mike','user','1');
INSERT INTO T_USER(NAME, TYPE, SEX) VALUES('Anna','user','0');
