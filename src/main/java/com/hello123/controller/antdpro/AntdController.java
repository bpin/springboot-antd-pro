package com.hello123.controller.antdpro;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @since 2020/01/26 22:27:41
 */
@Controller
@RequestMapping("/api")
public class AntdController {

	@RequestMapping("/auth_routes")
	public String auth_routes() {
		return "index";
	}

}
