package com.hello123.controller.demo;

import java.sql.Date;

import javax.xianfeng.antdpro.model.ApiResult;
import javax.xianfeng.antdpro.model.ApiResultEnums;
import javax.xianfeng.antdpro.model.PagingApiResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.hello123.model.demo.User;
import com.hello123.model.demo.UserDeleteDTO;
import com.hello123.service.demo.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * @since 2020/01/27 23:10:11
 */
@Slf4j
@RestController
@RequestMapping("/openapi/v2")
public class AntdResController2 {

	@Autowired
	private UserService userService;

	@GetMapping("/list")
	public Object list(Integer pageSize, Integer currentPage, String sortField, String sortDir, String type,
			String name, Byte sex, Date birthday, Integer age) {
		log.info("list pageSize = {}, currentPage = {}, sortField = {}, sortDir = {}", pageSize, currentPage, sortField,
				sortDir);
		log.info("type = {}, name = {}, sex = {], birthday = {}, age = {}", type, name, sex, birthday, age);
		
		PagingApiResult result = userService.list(pageSize, currentPage, sortField, sortDir, type, name, sex, birthday,
				age);
		
		return JSONObject.toJSON(result);
	}
	
	@GetMapping("/get")
	public Object get( Long id) {
		log.info("get id = {}", id);
		ApiResult result = userService.get(id);
		return JSONObject.toJSON(result);
	}

	@PostMapping("/add")
	public Object add(@RequestBody User user) {
		log.info("add user = {}", user);

		ApiResult result = null;
		try {
			userService.save(user);
			result = new ApiResult(ApiResultEnums.SUCCESS.getCode(), ApiResultEnums.SUCCESS.getText());
		} catch (Exception e) {
			result = new ApiResult(ApiResultEnums.FAILURE.getCode(), ApiResultEnums.FAILURE.getText());
		}

		return JSONObject.toJSON(result);
	}

	@PutMapping("/update")
	public Object update(@RequestBody User user) {
		log.info("update user = {}", user);

		ApiResult result = null;
		try {
			userService.update(user);
			result = new ApiResult(ApiResultEnums.SUCCESS.getCode(), ApiResultEnums.SUCCESS.getText());
		} catch (Exception e) {
			result = new ApiResult(ApiResultEnums.FAILURE.getCode(), ApiResultEnums.FAILURE.getText());
		}

		return JSONObject.toJSON(result);
	}

	@DeleteMapping("/remove")
	public Object remove(@RequestBody UserDeleteDTO dto) {
		log.info("remove dto = {}", dto);

		ApiResult result = null;
		try {
			userService.remove(dto.getId());
			result = new ApiResult(ApiResultEnums.SUCCESS.getCode(), ApiResultEnums.SUCCESS.getText());
		} catch (Exception e) {
			result = new ApiResult(ApiResultEnums.FAILURE.getCode(), ApiResultEnums.FAILURE.getText());
		}

		return JSONObject.toJSON(result);
	}

}
