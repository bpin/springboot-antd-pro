package javax.xianfeng.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 心跳检测
 * @since 2020/01/03 20:23:08
 */
@RestController
public class HealthMonitorController {

	@GetMapping("/ping")
	public String ping() {
		return "pong";
	}

}
